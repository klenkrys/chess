#include "Figures.h"
#include<iostream>

//----------ranks-----------
const int kingRank = 10000;
const int queenRank = 250;
const int bishopRank = 100;
const int knightRank = 100;
const int rockRank = 100;
const int pawnRank = 30;

//---------- KING -----------------------------------------------------------------
King::King(int side, Coordinates initCoordinates, int numOfStepsDone, int id)
 : Figure(1,'K',side,initCoordinates,numOfStepsDone, id, kingRank){
    mCastlePossible = true;
}
King::King(const King & ref)
 : Figure(ref.mSide, ref.mLetter,ref.mSide, ref.mCoordinates, ref.mNumOfStepsDone, ref.mId, kingRank ){

}

void King::getPossibleMovePositions(std::list<Coordinates> & possiblePositions){
    addMovesVertical(possiblePositions,*this);
    addMovesHorizontal(possiblePositions,*this);
    addMoves1Quadrant(possiblePositions,*this);
    addMoves2Quadrant(possiblePositions,*this);
    addMoves3Quadrant(possiblePositions,*this);
    addMoves4Quadrant(possiblePositions,*this);
    addCastlingPositionsForKing(possiblePositions,*this);
    //clearCheckMoves(possiblePositions,*this);
}
//---------- QUEEN -----------------------------------------------------------------
Queen::Queen(int side, Coordinates initCoordinates, int numOfStepsDone, int id)
 : Figure(8,'Q',side,initCoordinates,numOfStepsDone, id, queenRank){

}
Queen::Queen(const Queen & ref)
 : Figure(ref.mMaxNumberOfSteps, ref.mLetter, ref.mSide, ref.mCoordinates, ref.mNumOfStepsDone, ref.mId,queenRank ){

}

void Queen::getPossibleMovePositions(std::list<Coordinates> & possiblePositions){
    addMovesVertical(possiblePositions,*this);
    addMovesHorizontal(possiblePositions,*this);
    addMoves1Quadrant(possiblePositions,*this);
    addMoves2Quadrant(possiblePositions,*this);
    addMoves3Quadrant(possiblePositions,*this);
    addMoves4Quadrant(possiblePositions,*this);
}
//---------- BISHOP -----------------------------------------------------------------
Bishop::Bishop(int side, Coordinates initCoordinates, int numOfStepsDone, int id)
 : Figure(8,'B',side,initCoordinates,numOfStepsDone, id, bishopRank){

}
Bishop::Bishop(const Bishop & ref)
 : Figure( ref.mMaxNumberOfSteps , ref.mLetter, ref.mSide, ref.mCoordinates, ref.mNumOfStepsDone, ref.mId, bishopRank){

}

void Bishop::getPossibleMovePositions(std::list<Coordinates> & possiblePositions){
    addMoves1Quadrant(possiblePositions,*this);
    addMoves2Quadrant(possiblePositions,*this);
    addMoves3Quadrant(possiblePositions,*this);
    addMoves4Quadrant(possiblePositions,*this);
}
//---------- KNIGHT -----------------------------------------------------------------
Knight::Knight(int side, Coordinates initCoordinates, int numOfStepsDone, int id)
 : Figure(3,'H',side,initCoordinates,numOfStepsDone, id, knightRank){

}
Knight::Knight(const Knight & ref)
 : Figure(ref.mMaxNumberOfSteps, ref.mLetter, ref.mSide, ref.mCoordinates, ref.mNumOfStepsDone, ref.mId, knightRank ){

}

void Knight::getPossibleMovePositions(std::list<Coordinates> & possiblePositions){
    addMovesKnight(possiblePositions, *this);
}
//---------- ROCK -----------------------------------------------------------------
Rock::Rock(int side, Coordinates initCoordinates, int numOfStepsDone, int id) 
: Figure(8,'R',side,initCoordinates,numOfStepsDone, id, rockRank){

}
Rock::Rock(const Rock & ref) 
: Figure(ref.mMaxNumberOfSteps, ref.mLetter, ref.mSide, ref.mCoordinates, ref.mNumOfStepsDone, ref.mId, rockRank ){

}

void Rock::getPossibleMovePositions(std::list<Coordinates> & possiblePositions){
    addMovesVertical(possiblePositions,*this);
    addMovesHorizontal(possiblePositions,*this);
}
//---------- PAWN -----------------------------------------------------------------
Pawn::Pawn(int side, Coordinates initCoordinates,int numOfStepsDone, int id)
 : Figure(1,'P',side,initCoordinates,numOfStepsDone, id, pawnRank){
}
Pawn::Pawn(const Pawn & ref)
 : Figure(ref.mMaxNumberOfSteps, ref.mLetter, ref.mSide, ref.mCoordinates, ref.mNumOfStepsDone, ref.mId , pawnRank){

}
void Pawn::getPossibleMovePositions(std::list<Coordinates> & possiblePositions){
    
    if(this->mSide == 2){
        if(this->mCoordinates.mRowIndex - 1 >= 0){
            if(Board::playField[this->mCoordinates.mRowIndex - 1][this->mCoordinates.mColumnIndex].mIsFree)
                possiblePositions.push_back({ this->mCoordinates.mRowIndex - 1, this->mCoordinates.mColumnIndex});

            addQuadrantsForPawnSide2(possiblePositions, *this);
        }

        if(mNumOfStepsDone == 0 && Board::playField[this->mCoordinates.mRowIndex - 2][this->mCoordinates.mColumnIndex].mIsFree){
            possiblePositions.push_back({this->mCoordinates.mRowIndex - 2, this->mCoordinates.mColumnIndex });
        }

    }else{
        if(this->mCoordinates.mRowIndex + 1 < 8){
            if(Board::playField[this->mCoordinates.mRowIndex + 1][this->mCoordinates.mColumnIndex].mIsFree)
                possiblePositions.push_back({this->mCoordinates.mRowIndex + 1, this->mCoordinates.mColumnIndex});

            addQuadrantsForPawnSide1(possiblePositions, *this);
        }

        if(mNumOfStepsDone == 0 && Board::playField[this->mCoordinates.mRowIndex + 2][this->mCoordinates.mColumnIndex].mIsFree){
            possiblePositions.push_back({ this->mCoordinates.mRowIndex + 2, this->mCoordinates.mColumnIndex});
        }
    }
    addEnPassantForPawn(possiblePositions, *this);
}


void getFigureCopy(std::shared_ptr<Figure> ptrOriginal,std::shared_ptr<Figure>& pCopy ){
    char letter = ptrOriginal.get()->mLetter;
    int side = ptrOriginal.get()->mSide;
    int columnIndex = ptrOriginal.get()->mCoordinates.mColumnIndex;
    int rowIndex = ptrOriginal.get()->mCoordinates.mRowIndex;
    Coordinates coor = {rowIndex,columnIndex};
    int stepsDone = ptrOriginal.get()->mNumOfStepsDone;
    int id = ptrOriginal.get()->mId;

    switch (letter)
    {
    case 'K':
        pCopy = std::make_shared<King>(side,coor,stepsDone,id);
        break;
    case 'Q':
        pCopy =  std::make_shared<Queen>(side,coor,stepsDone,id);
        break;
    case 'B':
        pCopy =  std::make_shared<Bishop>(side,coor,stepsDone,id);
        break;
    case 'H':
        pCopy =  std::make_shared<Knight>(side,coor,stepsDone,id);
        break;
    case 'R':
        pCopy =  std::make_shared<Rock>(side,coor,stepsDone,id);
        break;
    default:
        pCopy =  std::make_shared<Pawn>(side,coor,stepsDone,id);
    }
}