#pragma once
#include "Figure.h" 
#include "Moves.h"
#include "Coordinates.h"

class King : public Figure{
public:
    King(int side, Coordinates initCoordinates, int numOfStepsDone, int id);
    King(const King & ref);
    void getPossibleMovePositions(std::list<Coordinates> & possiblePositions);
private:
    bool mCastlePossible;
};
class Queen :  public  Figure{
public:
    Queen(int side, Coordinates initCoordinates, int numOfStepsDone, int id);
    Queen(const Queen & ref);
    void getPossibleMovePositions(std::list<Coordinates> & possiblePositions);
};
class Bishop : public  Figure{
public:
    Bishop(int side, Coordinates initCoordinates, int numOfStepsDone, int id);
    Bishop(const Bishop & ref);
    void getPossibleMovePositions(std::list<Coordinates> & possiblePositions);
};
class Knight : public  Figure{
public:
    Knight(int side, Coordinates initCoordinates, int numOfStepsDone, int id);
    Knight(const Knight& ref);
    void getPossibleMovePositions(std::list<Coordinates> & possiblePositions);
};
class Rock : public  Figure{
public:
    Rock(int side, Coordinates initCoordinates, int numOfStepsDone, int id);
    Rock(const Rock & ref);
    void getPossibleMovePositions(std::list<Coordinates> & possiblePositions);
};
class Pawn : public  Figure{
public:
    Pawn(int side, Coordinates initCoordinates, int numOfStepsDone, int id);
    Pawn(const Pawn & ref);
    void getPossibleMovePositions(std::list<Coordinates> & possiblePositions);
};

void getFigureCopy(std::shared_ptr<Figure> ptrOriginal, std::shared_ptr<Figure>& pCopy );